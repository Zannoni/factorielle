package fr.ajc.formation.java;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalculateurfactorielleTest {

	@Test
	void resultatdefactoriellede0equal1() {
		assertEquals(1, CalculateurFactorielle.factorielle(1));
	}
	
	@Test
	void resultatdefactorielle2equal2() {
		assertEquals(2, CalculateurFactorielle.factorielle(2));
	}
	
	@Test
	void resultatdefactorielle3equal6() {
		assertEquals(5, CalculateurFactorielle.factorielle(3));
	}

}
